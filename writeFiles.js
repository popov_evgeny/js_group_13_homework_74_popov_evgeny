const fs = require('fs');

module.exports = {

  writeFile (message) {
    const fileName = `messages/${new Date()}`;
    fs.writeFile(fileName, `${JSON.stringify(message)}`, (err) => {
      if (err) {
        console.error(err);
      } else {
        console.log('File was saved!');
      }
    });
  }
}