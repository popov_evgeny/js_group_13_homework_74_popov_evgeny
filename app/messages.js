const express = require('express');
const writeFiles = require('../writeFiles');
const readFiles = require('../readFile');

const router = express.Router();

router.get('/', (req, res) => {
  let messages = readFiles.readFiles();
  return res.send( messages );
});

router.post('/', (req, res) => {
  const message = {
    message: req.body.message,
    date: new Date()
  };
  writeFiles.writeFile(message);
  return res.send(message);
});

module.exports = router;