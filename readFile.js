const fs = require('fs');
let messages = [];
let readFilesArray = [];
const path = "messages";

module.exports = {
  readFiles() {
  fs.readdir(path, (err, files) => {
    if (files.length >= 5) {
      readFilesArray =  files.slice(files.length - 5, files.length);
    } else {
       readFilesArray = files;
    }
    messages = [];
    readFilesArray.forEach(file => {
      fs.readFile((path + '/' + file), (err, data) => {
        if (err) {
          return console.error(err);
        } else {
          messages.push(JSON.parse(data));
        }
      });
    });
  });
  return  messages.sort( (a, b) => {
    if (a.date < b.date) {
      return -1;
    }
    if (a.date > b.date) {
      return 1;
    }
    return 0;
  });

}};